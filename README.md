# Time Register

## Tech

- PHP 8.1
- Postgres
- Laravel 8.75

## To run webserver

Install the libraries

``composer install``

copy .env.example to .env, and generate key secret

``php artisan key:generate``

and run serve

``php artisan serve``

will usually be running on localhost:8000

## Run tests

Copy .env.example to .env.testing, and create schema test on database

``php artisan migrate --env=testing``

add key secret

``php artisan key:generate``

and after

``php artisan test``
