<?php

use App\Http\Controllers\EmployeesController;
use Illuminate\Support\Facades\Route;

Route::get('/employees', [EmployeesController::class, 'index'])
    ->name('employees.index');
