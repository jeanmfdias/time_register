import React from "react";
import Authenticated from '@/Layouts/Authenticated';
import { Head } from "@inertiajs/inertia-react";

export default function Index(props) {
    function renderRowTable(item) {
        return (
            <tr key={item.id}>
                <td>{item.id}</td>
                <td>{item.name}</td>
            </tr>
        )
    }

    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Employees</h2>}
        >
            <Head title="Employees" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 bg-white border-b border-gray-200">
                            <table>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {props.employees.map(renderRowTable)}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
