<?php

namespace Tests\Unit;

use App\Models\Employee;
use App\Models\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EmployeesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * test to create an employee
     *
     * @return void
     */
    public function testCreateAnEmployee()
    {
        $faker = Factory::create();

        $user = User::factory()->create();

        $employee = new Employee();
        $employee->name = $faker->name();
        $employee->user_id = $user->id;
        $result = $employee->save();

        $this->assertTrue($result);
    }
}
