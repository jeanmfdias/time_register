<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Http\Requests\Employee\UpdateRequest as EmployeeUpdateRequest;
use App\Http\Requests\Employee\StoreRequest as EmployeeStoreRequest;
use Inertia\Inertia;

class EmployeesController extends Controller
{
    public function index()
    {
        $employees = Employee::all();

        return Inertia::render('Employees/Index', [
            'employees' => $employees
        ]);
    }

    public function show(Employee $employee)
    {

    }

    public function edit(Employee $employee)
    {

    }

    public function store(EmployeeStoreRequest $request)
    {

    }

    public function update(EmployeeUpdateRequest $request, Employee $employee)
    {

    }

    public function destroy(Employee $employee)
    {

    }
}
